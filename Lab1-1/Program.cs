﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Lab1_1
{
    class Program
    {
        private static TcpListener tcp;
        private static Socket s;
        static void Main(string[] args)
        {
            Console.WriteLine("Startuję serwer...");

            Serwus();
            string text = "";
            while (true)
            {
                Console.Write("Serwer: ");
                text = Console.ReadLine();
                if (text == "quit")
                    break;

                send(text);
                recive();
            }
            send(text);
            tcp.Stop();
        }

        static void Serwus()
        {
            tcp = new TcpListener(IPAddress.Parse("127.0.0.1"), 2222);

            tcp.Start();
            s = tcp.AcceptSocket();


            recive();
            //tcp.Stop();
        }

        static void recive()
        {
            
            byte[] oByte = new byte[100];

            int ret = s.Receive(oByte, oByte.Length, 0);
            byte[] oByte2 = oByte.Where(b => b > 0).ToArray();
            string tmp = "";
            tmp = Encoding.ASCII.GetString(oByte2);
            if (tmp.Length > 0)
            {
                if (tmp == "quit")
                {
                    Environment.Exit(-1);
                }
                Console.WriteLine("Odebrałem komunikat: " + tmp);
            }
        }
        static void send(string text)
        {
            Byte[] byteData = Encoding.ASCII.GetBytes(text.ToCharArray());
            s.Send(byteData, byteData.Length, 0);
        }
    }
}
