﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThrededHost
{
    class Program
    {
        private static TcpListener tcpLsn;
        private static Socket s;
        private static Thread reciving = new Thread(() => recive());
        private static Thread sending = new Thread(() => sendToSerwer());

        static void Main(string[] args)
        {
            Console.WriteLine("Hello,  This is host");
            connect();
            reciving.Start();
            sending.Start();

        }

        static void connect()
        {
            s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress hostAdr = IPAddress.Parse("127.0.0.1");
            int port = 2222;

            IPEndPoint EpHost = new IPEndPoint(hostAdr, port);
            s.Connect(EpHost);
        }
        static void recive()
        {
            try
            {
                while (true)
                {
                    byte[] oByte = new byte[100];
                    int ret = s.Receive(oByte, oByte.Length, 0);
                    byte[] oByte2 = oByte.Where(b => b > 0).ToArray();
                    string tmp = "";
                    tmp = Encoding.ASCII.GetString(oByte2);
                    if (tmp.Length > 0)
                    {
                        if (tmp == "quit")
                        {
                            Environment.Exit(-1);
                        }
                        Console.WriteLine("Serwer: " + tmp);
                    }
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                Environment.Exit(-1);
            }
        }

        private static void sendToSerwer()
        {
            string text = "";
            try
            {
                while (true)
                {
                    text = Console.ReadLine();
                    Console.Write("Klient: ");
                    Byte[] byteData = Encoding.ASCII.GetBytes(text.ToCharArray());
                    s.Send(byteData, byteData.Length, 0);
                    if (text == "quit")
                    {
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
