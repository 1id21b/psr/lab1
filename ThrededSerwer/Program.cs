﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThrededSerwer
{
    class Program
    {
        private static TcpListener tcp;
        private static Socket s;

        private static Thread reciving = new Thread(() => recive());
        private static Thread sending = new Thread(() => send());
        static void Main(string[] args)
        {
            Console.WriteLine("Startuję serwer...");

            Serwus();
            reciving.Start();
            sending.Start();


            tcp.Stop();
        }

        static void Serwus()
        {
            tcp = new TcpListener(IPAddress.Parse("127.0.0.1"), 2222);

            tcp.Start();
            s = tcp.AcceptSocket();
            //tcp.Stop();
        }

        static void recive()
        {
            try
            {
                while (true)
                {
                    byte[] oByte = new byte[100];
                    int ret = s.Receive(oByte, oByte.Length, 0);
                    byte[] oByte2 = oByte.Where(b => b > 0).ToArray();
                    string tmp = "";
                    tmp = Encoding.ASCII.GetString(oByte2);
                    if (tmp.Length > 0)
                    {
                        if (tmp == "quit")
                        {
                            Environment.Exit(-1);
                        }
                        Console.WriteLine("Klient: " + tmp);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                Environment.Exit(-1);
            }
        }
        static void send()
        {
            string text = "";
            try
            {
                while (true)
                {
                    text = Console.ReadLine();
                    Console.Write("Serwer: ");
                    Byte[] byteData = Encoding.ASCII.GetBytes(text.ToCharArray());
                    s.Send(byteData, byteData.Length, 0);
                    if (text == "quit")
                    {
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
