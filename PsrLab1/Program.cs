﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;


namespace PsrLab1
{
    class Program
    {
        private static TcpListener tcpLsn;
        private static Socket s;
        static void Main(string[] args)
        {
            Console.WriteLine("Hello,  This is host");
            connect();
            string text = "";
            while (true)
            {
                Console.Write("Klient: ");
                text = Console.ReadLine();
                if(text == "quit")
                {
                    break;
                }
                sendToSerwer(text);
                recive();
            }
            sendToSerwer(text);

        }

        static void connect()
        {
            s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress hostAdr = IPAddress.Parse("127.0.0.1");
            int port = 2222;

            IPEndPoint EpHost = new IPEndPoint(hostAdr, port);
            s.Connect(EpHost);
        }
        static void recive()
        {
            byte[] oByte = new byte[100];
            int ret = s.Receive(oByte, oByte.Length, 0);
            byte[] oByte2 = oByte.Where(b => b > 0).ToArray();
            string tmp = "";
            tmp = Encoding.ASCII.GetString(oByte2);
            if (tmp.Length > 0)
            {
                if (tmp == "quit")
                {
                    Environment.Exit(-1);
                }
                Console.WriteLine("Odebrałem komunikat: " + tmp);
            }
        }

        private static void sendToSerwer(string wiadomosc)
        {
            Byte[] byteData = Encoding.ASCII.GetBytes(wiadomosc.ToCharArray());
            s.Send(byteData, byteData.Length, 0);
        }
    }
}
